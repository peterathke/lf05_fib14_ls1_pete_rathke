
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte fuer x und y festlegen:
      // ===========================
      double x = 5.0;
      double y = 4.0;
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================

      m = berechnung(x, y);
   
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      
   }
   
  public static double berechnung (double p1, double p2) {
	  double ergebnis = (p1 + p2) / 2;
	  return ergebnis;
   }
   
   
}
