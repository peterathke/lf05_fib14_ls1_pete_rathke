import java.util.Scanner;

class Fahrkartenautomat
{
	
	private static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args)
    {
	
	    double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    fahrkartenAusgeben();
	    rueckgeldAusgeben(rueckgabebetrag);
    	
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
    }
    
    public static double fahrkartenbestellungErfassen() {
    	double ticketpreis;
    	double ticketanzahl;
    	double zuZahlenderBetrag;
    	
    	System.out.print("Geben Sie den Ticketpreis ein: ");
    	ticketpreis = tastatur.nextDouble();
    	
    	System.out.print("\nGeben Sie die Ticketanzahl ein: ");
    	ticketanzahl = tastatur.nextInt();
    	
    	zuZahlenderBetrag = ticketpreis * ticketanzahl;
    	
    	return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneM�nze;
    	double r�ckgabebetrag;
    	
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: " + "%.02f", zuZahlenderBetrag - eingezahlterGesamtbetrag);
     	   System.out.println(" Euro");
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;     
        }
        
        r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        
        return r�ckgabebetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\nDie Fahrkarten befinden sich im Ausgabekasten!\n\n");	
    }
    
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
    	
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }
    }
}

//5: Da die Anzahl an Tickets immer eine Nat�rliche Zahl ist, da nicht z.B. halbe Tickets gekauft werden k�nnen, kann hier "int" verwendet werden, um Bytes zu sparen.
//6: Der double zuZahlenderBetrag wird mit dem int anzahlTickets multipliziert und der Wert (das Ergebnis) wird dem neuen, vorher definierten double zuZahlenderGesamtbetrag zugewiesen.