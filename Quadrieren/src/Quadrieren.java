import java.util.Scanner;

public class Quadrieren {
   
	public static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		titel();
		double x = eingabe();
		double ergebnis = verarbeitung(x);
		ausgabe(x, ergebnis);
		
	}	
	
	public static void titel() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
	}
	
	public static double eingabe() {
		double x;
		
		System.out.print("Geben Sie eine Zahl ein: ");
		x = tastatur.nextDouble();
		
		return x;
	}
	
	public static double verarbeitung(double x) {
		double ergebnis;
		
		ergebnis = x * x;
		
		return ergebnis;
	}
	
	public static void ausgabe(double x, double ergebnis) {
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}
	
}
