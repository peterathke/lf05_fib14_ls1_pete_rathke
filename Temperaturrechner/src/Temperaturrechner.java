import java.util.Scanner;

public class Temperaturrechner {

	public static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.println("Willkommen beim Temperaturrechner!\n----------------------------------\n\n");
		
	char nochmal ='J';
		
	while(nochmal == 'J') {
		
		System.out.print("Bitte geben Sie die Ausgangseinheit ein: ");
		char ausgangseinheit = tastatur.next().charAt(0);
		ausgangseinheit = Character.toUpperCase(ausgangseinheit);
		
		while(falscheeingabe(ausgangseinheit)) {
			System.out.print("\nDiese Einheit ist ung�ltig.\nBitte geben Sie eine g�ltige Einheit ein (m�gliche Einheiten: Celsius, Fahrenheit, Kelvin): ");
			ausgangseinheit = tastatur.next().charAt(0);
			ausgangseinheit = Character.toUpperCase(ausgangseinheit);
		}
		
		System.out.print("\nBitte geben Sie die Zieleinheit ein: ");
		char zieleinheit = tastatur.next().charAt(0);
		zieleinheit = Character.toUpperCase(zieleinheit);
		
		while(falscheeingabe(zieleinheit) || zieleinheit == ausgangseinheit) {
			if(zieleinheit == ausgangseinheit) {
				System.out.print("\nDie Zieleinheit darf nicht mit der Ausgangseinheit �bereinstimmen!");
			}
				else {
					System.out.print("\nDiese Einheit ist ung�ltig.\nBitte geben Sie eine g�ltige Einheit ein (m�gliche Einheiten: Celsius, Fahrenheit, Kelvin): ");
				}
			zieleinheit = tastatur.next().charAt(0);
			zieleinheit = Character.toUpperCase(zieleinheit);
		}
		
		double ergebnis = 0;
		
		System.out.print("\nBitte geben Sie die Ausgangsgradzahl ein: ");
		double eingabe = tastatur.nextDouble();
		
		if(ausgangseinheit == 'C' && zieleinheit == 'F') {
			ergebnis = celsiusinfahrenheit(eingabe);
		}
		
			else if(ausgangseinheit == 'C' && zieleinheit == 'K') {
				ergebnis = celsiusinkelvin(eingabe);
			}
		
				else if(ausgangseinheit == 'F' && zieleinheit == 'C') {
					ergebnis = fahrenheitincelsius(eingabe);
				}
		
					else if(ausgangseinheit == 'F' && zieleinheit == 'K') {
						ergebnis = fahrenheitinkelvin(eingabe);
					}
		
						else if(ausgangseinheit == 'K' && zieleinheit == 'C') {
							ergebnis = kelvinincelsius(eingabe);
						}
		
							else {
								ergebnis = kelvininfahrenheit(eingabe);
							}
		
		ausgabe(eingabe, ergebnis, ausgangseinheit, zieleinheit);
		
		System.out.print("\n\n\nWollen Sie eine weitere Berechnung durchf�hren? ");
		nochmal = tastatur.next().charAt(0);
		nochmal = Character.toUpperCase(nochmal);
		System.out.println("\n");
		
		}
		
	System.out.print("------------------------------------------------------\nDanke, dass Sie den Temperaturrechner verwendet haben!");
	
	}
	
	public static boolean falscheeingabe(char eingabe) {
		return eingabe != 'C' && eingabe != 'F' && eingabe != 'K';
	}
	
	public static double celsiusinfahrenheit(double eingabe) {
		return eingabe * 1.8 + 32;
	}
	
	public static double celsiusinkelvin(double eingabe) {
		return eingabe + 273.15;
	}
	
	public static double fahrenheitincelsius(double eingabe) {
		return (eingabe - 32) / 1.8;
	}
	
	public static double fahrenheitinkelvin(double eingabe) {
		return (eingabe - 32) / 1.8 + 273.15;
	}
	
	public static double kelvinincelsius(double eingabe) {
		return eingabe - 273.15;	
	}
	
	public static double kelvininfahrenheit(double eingabe) {
		return (eingabe - 273.15) * 1.8 + 32;	
	}
	
	public static void ausgabe(double eingabe, double ergebnis, char ausgangseinheit, char zieleinheit) {
		if(ausgangseinheit == 'K') {
			System.out.printf("\n\nIhr Ausgangswert: %.2f " + ausgangseinheit + ". Ihr Zielwert: %.2f " + "�" + zieleinheit + ".", eingabe, ergebnis);
		}
			else if(zieleinheit == 'K') {
				System.out.printf("\n\nIhr Ausgangswert: %.2f �" + ausgangseinheit + ". Ihr Zielwert: %.2f " + zieleinheit + ".", eingabe, ergebnis);
			}
				else {
					System.out.printf("\n\nIhr Ausgangswert: %.2f �" + ausgangseinheit + ". Ihr Zielwert: %.2f �" + zieleinheit + ".",eingabe, ergebnis);
				}
		
	}
	
}
