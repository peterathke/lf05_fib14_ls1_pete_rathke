import java.util.Scanner;

public class aufgabe4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner myScanner = new Scanner(System.in);
		
		double rabatt10 = 10;
		double rabatt15 = 15;
		double rabatt20 = 20;
		
		System.out.println("Hallo! Mit diesem Programm k�nnen Sie nach Eingabe Ihres Bestellwerts in Erfahrung bringen,\nwelcher Prozentsatz bei der Rabattberechnung f�r Ihre Bestellung g�ltig ist und wie hoch Ihr erm��igter Bestellwert (incl. MwSt.) ist.");
		System.out.print("\nBitte geben Sie jetzt Ihren Bestellwert ein! ");
			double bestwert = myScanner.nextDouble();
			
		System.out.print("Der eingegebene Bestellwert liegt bei: ");
		System.out.printf("%.2f", bestwert);
		System.out.println(" Euro.\n");
		
		if (bestwert <= 0) {
			System.out.println("Der eingegebene Betrag ist ung�ltig.");
		}
		
		else if (bestwert > 0 && bestwert <= 100) {
			System.out.println("Der f�r Ihre Bestellung g�ltige Rabatt liegt bei: " + rabatt10 + "%.");
			double endwert = bestwert * (1 - rabatt10 /100) * 1.19;
			System.out.println("Der erm��igte Bestellwert (incl.MwSt.) liegt bei: " + endwert + " Euro.");
		}
		
		else if (bestwert > 100 && bestwert < 500) {
			System.out.println("Der f�r Ihre Bestellung g�ltige Rabatt liegt bei: " + rabatt15 + "%.");
			double endwert = bestwert * (1 - rabatt15 /100) * 1.19;
			System.out.println("Der erm��igte Bestellwert (incl.MwSt.) liegt bei: " + endwert + " Euro.");
		}
		
		else {
			System.out.println("Der f�r Ihre Bestellung g�ltige Rabatt liegt bei: " + rabatt20 + "%.");
			double endwert = bestwert * (1 - rabatt20 /100) * 1.19;
			System.out.println("Der erm��igte Bestellwert (incl.MwSt.) liegt bei: " + endwert + " Euro.");
		}
		
		myScanner.close();
		
	}
	
}

