public class Konsolenasugabe2 {
	public static void main(String[] args) {
		//Aufgabe 1
			String s = "*";
				System.out.printf( "%5s\n", s + s );
				System.out.printf( "%s%7s\n", s, s );
				System.out.printf( "%s%7s\n", s, s );
				System.out.printf( "%5s\n", s + s );
			System.out.print("\n");
		//Aufgabe 2
			String a0 = "0!";
			String a1 = "1!";
			String a2 = "2!";
			String a3 = "3!";
			String a4 = "4!";
			String a5 = "5!";
			String b0 = "";
			String b1 = " 1";
			String b2 = " 1 * 2";
			String b3 = " 1 * 2 * 3";
			String b4 = " 1 * 2 * 3 * 4";
			String b5 = " 1 * 2 * 3 * 4 *5";
			String c01 = "1";
			String c2 = "2";
			String c3 = "6";
			String c4 = "24";
			String c5 = "120";
				System.out.printf( "%-5s=%-19s=%4s\n", a0, b0, c01);
				System.out.printf( "%-5s=%-19s=%4s\n", a1, b1, c01);
				System.out.printf( "%-5s=%-19s=%4s\n", a2, b2, c2);
				System.out.printf( "%-5s=%-19s=%4s\n", a3, b3, c3);
				System.out.printf( "%-5s=%-19s=%4s\n", a4, b4, c4);
				System.out.printf( "%-5s=%-19s=%4s\n", a5, b5, c5);
			System.out.print("\n");	
		//Aufgabe 3
			String x = "Fahrenheit";
			String y = "Celsius";
			double x1 = -20;
			double x2 = -10;
			double x3 = +0;
			double x4 = +20;
			double x5 = +30;
			double y1 = -1.1111;
			double y2 = -23.3333;
			double y3 = -17.7778;
			double y4 = -6.6667;
			double y5 = -1.1111;
				System.out.printf( "%-12s|%10s\n", x, y);
				System.out.print("-----------------------\n");
				System.out.printf( "%-12s|%10.2f\n", x1, y1);
				System.out.printf( "%-12s|%10.2f\n", x2, y2);
				System.out.printf( "%-12s|%10.2f\n", x3, y3);
				System.out.printf( "%-12s|%10.2f\n", x4, y4);
				System.out.printf( "%-12s|%10.2f\n", x5, y5);
	}
}